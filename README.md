# **TCGA GERMLINE MAPPING**

Here I share all code used within the project:


#### Original file hosted in the cluster:

This file was downloaded using GDC client tool

/storage/scratch01/users/pgonzalezd/TCGA-TGermline/input/PCA.r1.TCGAbarcode.merge.tnSwapCorrected.10389.vcf.gz

This file is ~ 27 GB. Be careful, the uncompressed one is ~ 3TB!

# steps:
## 1- create index to facilitate handling

`bcftools index PCA.r1.TCGAbarcode.merge.tnSwapCorrected.10389.vcf.gz`

## 2- split original file along with the index to generate one vcf.gz file per chromosome:

#### input: 
- PCA.r1.TCGAbarcode.merge.tnSwapCorrected.10389.vcf.gz + PCA.r1.TCGAbarcode.merge.tnSwapCorrected.10389.vcf.gz.csi

#### scripts:
- split_per_chromosome.sbatch  
- split_per_sex_chromosome.sbatch

#### output:
- /storage/scratch01/users/pgonzalezd/TCGA-TGermline/input/


## 3- convert multialllelic to biallelic variants + create unique ID=CHROM_POS_REF_ALT 

### input:
- /storage/scratch01/users/pgonzalezd/TCGA-TGermline/input/chr{}.vcf.gz

### script:
- bcftools.sbatch

### output:
- /storage/scratch01/users/pgonzalezd/TCGA-TGermline/TCGA/ready_vcf
- chr{}.ready.vcf.gz

## 4- annotation
This files (chr{}.ready.vcf.gz) undergo annotation and vcf2tsv conversion

- pre-step. We only need first 9 column of the vcf's to annotate

`cut -f 1-9 chr{}.ready.vcf.gz >> chr{}.annovar.vcf`

### annovar:
### input: 
- chr{}.annovar.vcf

### script:
- annotate_arrays.sbatch

### output:
- /storage/scratch01/users/pgonzalezd/TCGA-TGermline/TCGA/ready_vcf/annotations
- chr{}.hg19_multianno.txt

### vep:
### input:
- chr{}.annovar.vcf

### script:
- vepelayo snakemake
- https://gitlab.com/bio.pelayo/vepelayo

### output:
- chr{}.txt


## 5- vcf2tsv conversion

### input: 
- chr{}.ready.vcf.gz

### script:
- https://gitlab.com/bio.pelayo/pelayosomas

### output: 
- chr{}.tsv

## 6- merging steps:
### load additional resources:

- script: rmw.R


## 7- use single final mapping file in the merging steps to use final mapping snakemake

#### Final mapping snakemake will map the file selected along with the output of pelayosomas

### input:
final mapping file (p.e def.CPG.exonic.functional.both.cov.map.TCGA.af.flag.wes.wgs.txt) + pelayosomas output

### script:
snakemake /home/pgonzalezd/final_mapping

### output 
final mapping file selected with sample information
(p.e, def.CPG.exonic.functional.both.cov.map.TCGA.af.flag.wes.wgs.txt + sample information)







